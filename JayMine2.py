import sys
from background2 import Background
import argparse
import os
import pygame
from colors import colors

if __name__ == '__main__':
  os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"

  parser = argparse.ArgumentParser(description = 'Set params for your custom Minesweeper game!')
  parser.add_argument('-p', help = 'Mine coverage percentage. Default-0,2.', type=float)
  parser.add_argument('-g', help = 'Grid size (x^2). Default-10, Max-20, Min-5', type=int)
  args = parser.parse_args()

  g = args.g or 10
  p = args.p or 0.2

  pygame.init()

  font = pygame.font.SysFont('Comic Sans MS', 15)
  IMG_bomb = pygame.image.load("mine.png")
  IMG_flag = pygame.image.load("flag.png")

  bar_height = 30
  grid_size = {"width":30*g, "height":30*g} 
  cell_size = {"width":grid_size.get("width")//g, "height":grid_size.get("height")//g}
  window_size = (grid_size.get("width"), grid_size.get("height") + bar_height)

  b = Background(g, p)

  screen = pygame.display.set_mode(size=window_size)

  IMG_bomb = pygame.transform.scale(IMG_bomb, (cell_size.get("width")-1, cell_size.get("height")-1))
  IMG_flag = pygame.transform.scale(IMG_flag, (cell_size.get("width")-1, cell_size.get("height")-1))

  while True:
    for event in pygame.event.get():
      if event.type == pygame.QUIT: sys.exit()
      if event.type == pygame.KEYUP:
        if event.key in (pygame.K_DOWN, pygame.K_s):
          b.go_down()
        if event.key in (pygame.K_UP, pygame.K_w):
          b.go_up()
        if event.key in (pygame.K_RIGHT, pygame.K_d):
          b.go_right()
        if event.key in (pygame.K_a, pygame.K_LEFT):
          b.go_left()

      if event.type == pygame.MOUSEBUTTONUP:
        pos = pygame.mouse.get_pos()
        x, y = pos[0]//cell_size.get("width"), (pos[1]-bar_height)//cell_size.get("height")
        if event.button == 1:
          if b.check_revealed((x,y)) or b.check_flag((x,y)):
            super
          else:
            b.add_revealed((x,y))
        elif event.button == 3:
          if b.check_flag((x,y)):
            b.remove_flag((x,y))
          else:
            b.add_flag((x,y))

    score_surf = font.render(b.get_score(), True, colors.get('black'))
    pygame.draw.rect(screen, colors.get('white'), (1, 1, grid_size.get("width"), bar_height))

    screen.blit(score_surf,(1,1))
    for background_x_iter, display_x_iter in zip(b.get_x_grid_range(), range(g)):
      for background_y_iter, display_y_iter in zip(b.get_y_grid_range(), range(g)):
        if b.check_revealed((display_x_iter,display_y_iter)):
          revealed_value = b.grid.get((background_x_iter, background_y_iter))
          if revealed_value == 9:
            pygame.draw.rect(   screen,
                                colors['white'], 
                                (   
                                    display_x_iter*cell_size.get("width")+1,             #position x
                                    display_y_iter*cell_size.get("height")+bar_height+1, #position y
                                    cell_size.get("width"),                              #size x
                                    cell_size.get("height")                              #size y
                                )
                            )

            screen.blit(IMG_bomb,
                        (
                            display_x_iter*cell_size.get("width")+1,             #position x
                            display_y_iter*cell_size.get("height")+bar_height+1  #position y
                        ))
        
            b.new_game()
          else:
            if sum((background_x_iter, background_y_iter)) % 2 == 0:
              pygame.draw.rect(screen, 
                              colors.get('dark-gray-field'), 
                              (
                                  display_x_iter*cell_size.get("width")+1,             #position x
                                  display_y_iter*cell_size.get("height")+bar_height+1, #position y
                                  cell_size.get("width"),                              #size x
                                  cell_size.get("height")                              #size y
                              ))
            else:
              pygame.draw.rect(screen, 
                              colors.get('light-gray-field'), 
                              (
                                  display_x_iter*cell_size.get("width")+1,             #position x
                                  display_y_iter*cell_size.get("height")+bar_height+1, #position y
                                  cell_size.get("width"),                              #size x
                                  cell_size.get("height")                              #size y
                              ))
            if revealed_value!=0 and revealed_value is not None:
              textSurf = font.render(str(revealed_value), True, colors.get(revealed_value))
              screen.blit(textSurf,
                          (
                              display_x_iter*cell_size.get("width")+1,             #position x
                              display_y_iter*cell_size.get("height")+bar_height+1  #position y
                          ))
        else:
          if sum((background_x_iter, background_y_iter)) %2 ==0:
            pygame.draw.rect(screen, 
                              colors.get('dark-green-field'), 
                              (
                                  display_x_iter*cell_size.get("width")+1,             #position x
                                  display_y_iter*cell_size.get("height")+bar_height+1, #position y
                                  cell_size.get("width"),                              #size x
                                  cell_size.get("height")                              #size y
                              ))
          else:
            pygame.draw.rect(screen, 
                              colors.get('light-green-field'), 
                              (
                                  display_x_iter*cell_size.get("width")+1,                #position x
                                  display_y_iter*cell_size.get("height")+bar_height+1,    #position y
                                  cell_size.get("width"),                                 #size x
                                  cell_size.get("height")                                 #size y
                              ))
          if b.check_flag((display_x_iter,display_y_iter)):
            screen.blit(IMG_flag,
                          (
                              display_x_iter*cell_size.get("width")+1,            #position x
                              display_y_iter*cell_size.get("height")+bar_height+1 #position y
                          ))
    pygame.display.flip()
