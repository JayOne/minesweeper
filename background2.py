import random as r
import numpy  as np
import math   as m
import bisect as b

class Background():
  def __init__(self, grid_size, bomb_occurrence_perc):
    self.grid_size = grid_size
    self.rand = r.randint(0,100)
    self.bomb_occurrence = int(bomb_occurrence_perc*100)
    self.bomb_count = round(bomb_occurrence_perc*pow(self.grid_size, 2))
    self.base_coords = {'x':0,'y':0}
    self.coords = {'x':0,'y':0}
    self.revealed = set()
    self.flags = set()
    self.bombs = set()
    self.grid = {}
    self.score = 0
    self.set_bombs()
    self.set_grid()

  def new_game(self):
    self.rand = r.randint(0,100)
    self.base_coords = {'x':0,'y':0}
    self.coords = {'x':0,'y':0}
    self.revealed = set()
    self.flags = set()
    self.bombs = set()
    self.grid = {}
    self.score = 0
    self.set_bombs()
    self.set_grid()

  def update_base_coords(self):
    self.base_coords.update({'x':self.coords.get('x')-self.coords.get('x')%self.grid_size}) 
    self.base_coords.update({'y':self.coords.get('y')-self.coords.get('y')%self.grid_size})

  def update(self):
    self.update_base_coords()
    self.grid.clear()
    self.bombs.clear()
    self.set_bombs()
    self.set_grid()
    self.update_revealed()

  def go_left(self):
    self.coords.update({'x':self.coords.get('x') - 1})
    self.update()

  def go_right(self):
    self.coords.update({'x':self.coords.get('x') + 1})
    self.update()

  def go_down(self):
    self.coords.update({'y':self.coords.get('y') - 1})
    self.update()
    
  def go_up(self):
    self.coords.update({'y':self.coords.get('y') + 1})
    self.update()

  def get_real_coords_from_relevant(self,coords):
    x_rel, y_rel = coords
    x_real = self.coords.get('x') + x_rel
    y_real = self.coords.get('y') + self.grid_size - y_rel - 1
    return (x_real, y_real)

  def check_revealed(self, coords):
    if self.get_real_coords_from_relevant(coords) in self.revealed:
      return True
    return False

  def update_revealed(self):
    curr_x, curr_y = self.coords.values()
    for i in range(curr_x, curr_x+self.grid_size):
      for j in range(curr_y, curr_y+self.grid_size):
        if (i,j) in self.revealed and self.grid.get((i,j)) == 0:
          self.reveal((i,j))

  def reveal(self, coords):
    tmp_x, tmp_y = coords
    curr_x, curr_y = self.coords.values()
    for i in range(max(tmp_x-1, curr_x),min(tmp_x+2, curr_x + self.grid_size)):
        for j in range(max(tmp_y-1, curr_y),min(tmp_y+2, curr_y + self.grid_size)):       
            if (i,j) not in self.revealed:
                if self.grid.get((i,j)) == 0:
                    self.add_revealed((i,j), False)
                    self.reveal((i,j))
                else:
                    self.add_revealed((i,j), False)

  def add_revealed(self, coords, relative=True):
    real_coords = coords
    if relative:
      real_coords = self.get_real_coords_from_relevant(coords)
    self.revealed.add(real_coords)
    if self.grid.get(real_coords) == 0:
      self.reveal(real_coords)
    self.score += self.grid.get(real_coords)*self.bomb_occurrence + 1

  def remove_flag(self, coords):
    self.flags.discard(self.get_real_coords_from_relevant(coords))

  def check_flag(self,coords):
    if self.get_real_coords_from_relevant(coords) in self.flags:
      return True
    return False

  def add_flag(self,coords):
    self.flags.add(self.get_real_coords_from_relevant(coords))

  def get_score(self):
    return str(self.score)

  def get_y_grid_range(self):
    return range(self.coords.get('y') + self.grid_size-1, self.coords.get('y')-1, -1)

  def get_x_grid_range(self):
    return range(self.coords.get('x'), self.coords.get('x') + self.grid_size)


  def set_bombs(self):
    base_x, base_y = self.base_coords.values()
    offset = self.grid_size
    coords = {"n_w":(base_x-offset,base_y+offset),"n_e":(base_x+offset,base_y+offset),"n":(base_x,base_y+offset),
              "e":(base_x+offset,base_y),"w":(base_x-offset,base_y),"s":(base_x,base_y-offset),"s_e":(base_x+offset,base_y-offset),
              "s_w":(base_x-offset,base_y-offset),"c":(base_x, base_y)}
    self.bombs.clear()
    for coord in coords.values():
      tmp_x, tmp_y = coord
      r.seed(sum((self.rand,(tmp_x<<32),tmp_y)))
      sample_list = r.sample(range(0,pow(self.grid_size,2)),k=self.bomb_count)
      for sample in sample_list:
        self.bombs.add((tmp_x + sample % self.grid_size, tmp_y + sample // self.grid_size))

  def calculate_area(self,coords):
    tmp_x, tmp_y = coords
    area_bomb_count = 0
    for x_ in range(tmp_x-1,tmp_x+2):
      for y_ in range(tmp_y-1,tmp_y+2):
        if (x_, y_) in self.bombs:
          area_bomb_count += 1
    return area_bomb_count

  def set_grid(self):
    base_x, base_y = self.base_coords.values()
    curr_x, curr_y = self.coords.values()
    for x_ in range(base_x-self.grid_size,base_x+2*self.grid_size):
      for y_ in range(base_y-self.grid_size,base_y+2*self.grid_size):
        if (x_,y_) in self.bombs:
          self.grid.update({(x_,y_):9})
        elif x_ in range(curr_x,curr_x+self.grid_size) and y_ in range(curr_y,curr_y+self.grid_size) :
          self.grid.update({(x_,y_):self.calculate_area((x_,y_))})
