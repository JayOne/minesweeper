Infinite minesweeper game

Run with optional arguments:<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-p P&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mine coverage percentage. Default - 0,2<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-g G&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Grid size (x^2). Default - 10, Max - 20, Min - 5
    
